package com.zuzu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App 
{
    private static final Logger log = LogManager.getLogger();

    public static void main( String[] args )
    {
        log.info("Salut la patzani!");
        log.error("opana, o eroare \"careva\'");
        log.error("ops", new RuntimeException("omg, WhataTerribleFailure"));
    }
}
